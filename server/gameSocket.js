class GameSocketHandler {
  constructor(io, game) {
    this.io = io;
    this.game = game;
  }
  init() {
    const gameStateRepeater = setInterval(this.gameStateUpdateInterval.bind(this), 80);
  }
  gameStateUpdateInterval() {
    this.manageGameFrame();
    const gameState = {
      snakes: this.getAllSnakes(),
      fruitPos: this.game.fruitPos
    };
    this.io.in('snake').emit('gameStateUpdate', gameState);
  }
  manageGameFrame() {
    const clients = Object.keys(this.io.sockets.in('snake').sockets);
    clients.forEach(clientId => {
      if (typeof(this.io.sockets.in('snake').sockets[clientId].user) !== 'undefined' && this.io.sockets.in('snake').sockets[clientId].user.active === true) {
        this.game.manageFramePerSnake(this.io.sockets.in('snake').sockets[clientId].user);
      }
    });
  }
  onUpdateDirection(directionKey, socket) {
    if (this.game.directionGuard(this.game.mapKeyToDirection(directionKey), socket.user.snake.direction) === true) {
      socket.user.snake.direction = this.game.mapKeyToDirection(directionKey);
    }
  }
  onDisconnect(socket) {
    if (typeof(socket.user) !== 'undefined') {
      socket.broadcast.to('snake').emit('userDisconnect', {name: socket.user.name});
    }
  }
  onLogin(name, socket) {
    const user = this.game.setNewUser(name, socket.id);
    socket.user = user;
    socket.join('snake');
    socket.broadcast.to('snake').emit('login', user);
    socket.emit('loginHandshake', user);
  }
  getAllSnakes() {
    const clients = Object.keys(this.io.sockets.in('snake').sockets);
    let snakes = {};
    clients.forEach(clientId => {
      if (typeof(this.io.sockets.in('snake').sockets[clientId].user) !== 'undefined') {
        snakes[clientId] = this.io.sockets.in('snake').sockets[clientId].user.snake;
      }
    });
    return snakes;
  }
}

Object.assign(GameSocketHandler.prototype, {
  io: null,
  game: null
});

module.exports.GameSocketHandler = GameSocketHandler;
