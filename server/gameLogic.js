const GameConfig = require('../src/app/config');
const Snake = require('./snake');

class GameLogicHandler {
  constructor() {
    this.conf = GameConfig.Config;
    this.conf.width = GameConfig.Config.COLS;
    this.conf.height = GameConfig.Config.ROWS;
    this.snakes = [];
    this.fruitPos = {};
  }
  init() {
    this.fruitPos = this.findFreePos();
  }
  doesSnakeBiteItself(user) {
    const snake = user.snake;
    const position = snake.last;
    let bitten = false;
    snake._queue.forEach((pos, index) => {
      if (index > 2 && pos.x === position.x && pos.y === position.y) {
        bitten = true;
      }
    });
    if (bitten === true) {
      user.active = false;
    }
  }
  manageFramePerSnake(user) {
    const snake = user.snake;

    snake.last = this.moveSnakeToNext(snake.last.x, snake.last.y, snake.direction);
    this.doesSnakeAteFruit(snake);
    snake.insert(snake.last.x, snake.last.y);
    snake.remove();

    this.doesSnakeBiteItself(user);
  }
  doesSnakeAteFruit(snake) {
    if (this.isPosFruit({x: snake.last.x, y: snake.last.y}) === true) {
      snake.insert(snake.last.x, snake.last.y);
      this.updateFruit();
      snake.score++;
    }
  }
  moveSnakeToNext(x, y, direction) {
    switch (direction) {
      case this.conf.LEFT:
        if (x > 0) {
          x--;
        } else {
          x = this.conf.width - 1;
        }
        break;
      case this.conf.UP:
        if (y > 0) {
          y--;
        } else {
          y = this.conf.height - 1;
        }
        break;
      case this.conf.RIGHT:
        if (x < this.conf.width - 1) {
          x++;
        } else {
          x = 0;
        }
        break;
      case this.conf.DOWN:
        if (y < this.conf.height - 1) {
          y++;
        } else {
          y = 0;
        }
        break;
      default:
        break;
    }
    return {x, y};
  }
  updateFruit() {
    this.fruitPos = this.findFreePos();
  }
  findFreePos() {
    const empty = [];
    for (let x = 0; x < this.conf.width; x++) {
      for (let y = 0; y < this.conf.height; y++) {
        if (this.isPosSnake({x, y}) === false || this.isPosFruit({x, y}) === false) {
          empty.push({x, y});
        }
      }
    }
    return empty[Math.floor(Math.random() * empty.length)];
  }
  isPosSnake(pos) {
    let result = false;
    this.snakes.forEach(snake => {
      if (result === false) {
        snake._queue.forEach(snakePos => {
          if (pos.x === snakePos.x && pos.y === snakePos.y) {
            result = true;
          }
        });
      }
    });
    return result;
  }
  isPosFruit(pos) {
    return pos.x === this.fruitPos.x && pos.y === this.fruitPos.y;
  }
  directionGuard(direction, snakeDirection) {
    if (direction === this.conf.RIGHT && snakeDirection === this.conf.LEFT) {
      return false;
    } else if (direction === this.conf.LEFT && snakeDirection === this.conf.LEFT) {
      return false;
    } else if (direction === this.conf.UP && snakeDirection === this.conf.DOWN) {
      return false;
    } else if (direction === this.conf.DOWN && snakeDirection === this.conf.UP) {
      return false;
    }
    return true;
  }
  mapKeyToDirection(key) {
    if (key === this.conf.KEY_UP) {
      return this.conf.UP;
    } else if (key === this.conf.KEY_DOWN) {
      return this.conf.DOWN;
    } else if (key === this.conf.KEY_RIGHT) {
      return this.conf.RIGHT;
    } else if (key === this.conf.KEY_LEFT) {
      return this.conf.LEFT;
    }
  }
  randomDirection() {
    const directions = [this.conf.LEFT, this.conf.RIGHT, this.conf.UP, this.conf.DOWN];
    return directions[Math.floor(Math.random() * directions.length)];
  }
  setNewUser(name, socketId) {
    const pos = this.findFreePos();
    const direction = this.randomDirection();
    const userSnake = new Snake.Snake(direction, pos.x, pos.y);
    userSnake.init();

    const user = {
      name: name.name,
      id: socketId,
      snake: userSnake,
      active: true,
      score: 0
    };
    return user;
  }
}

Object.assign(GameLogicHandler.prototype, {
  conf: {},
  snakes: [],
  fruitPos: {}
});

module.exports.GameLogicHandler = GameLogicHandler;
