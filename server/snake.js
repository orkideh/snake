class Snake {
  constructor(direction, x, y) {
    this.direction = direction;
    this.x = x;
    this.y = y;
    this.score = 0;
  }
  init() {
    this._queue = [];
    this.insert(this.x, this.y);
  }
  insert(x, y) {
    this._queue.unshift({x, y});
    this.last = this._queue[0];
  }
  remove() {
    return this._queue.pop();
  }
}

Object.assign(Snake.prototype, {
  direction: null,
  _queue: null,
  last: null,
  score: 0
});

module.exports.Snake = Snake;
