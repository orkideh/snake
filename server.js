const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const GameLogicHandler = require('./server/gameLogic');
const GameSocketHandler = require('./server/gameSocket');

const Game = new GameLogicHandler.GameLogicHandler();
Game.init();

app.use(express.static(__dirname + '/dist/'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/dist/index.html');
});

const GameSocket = new GameSocketHandler.GameSocketHandler(io, Game);
GameSocket.init();

io.on('connection', socket => {
  socket.on('login', name => GameSocket.onLogin(name, socket));
  socket.on('updateDirection', directionKey => GameSocket.onUpdateDirection(directionKey, socket));
  socket.on('disconnect', () => GameSocket.onDisconnect(socket));
});

http.listen(8081, () => {
  console.log('listening on *:8081');
});
