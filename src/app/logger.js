class Logger {
  constructor(ws, container) {
    this.ws = ws;
    this.container = container;
  }
  init() {
    this.ws.on('login', user => {
      const msg = user.name + ' just joined the game';
      this.appendMessage(msg);
    });
    this.ws.on('userDisconnect', msg => {
      msg = msg.name + ' Has been disconnected';
      this.appendMessage(msg);
    });
    this.ws.on('score', msg => {
      msg = msg.name + ' gained score: ' + msg.score;
      this.appendMessage(msg);
    });
  }
  appendMessage(message) {
    const messageElem = document.createElement('p');
    const textNode = document.createTextNode(message);
    messageElem.appendChild(textNode);
    this.container.appendChild(messageElem);
  }
}

Object.assign(Logger.prototype, {
  ws: null,
  container: null
});

module.exports.Logger = Logger;
