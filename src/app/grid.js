class Grid {
  init(defaultValue, width, height) {
    this.width = width;
    this.height = height;

    this._grid = [];
    for (let col = 0; col < this.width; col++) {
      this._grid.push(new Array(this.height).fill(defaultValue));
    }
  }
  set(val, x, y) {
    this._grid[x][y] = val;
  }
  get(x, y) {
    return this._grid[x][y];
  }
}

Object.assign(Grid.prototype, {
  width: null,
  height: null,
  _grid: null
});

module.exports.Grid = Grid;

