const Config = {
  COLS: 25,
  ROWS: 25,
  EMPTY: 0,
  SNAKE: 1,
  FRUIT: 2,
  GUEST_SNAKE: 3,
  LEFT: 0,
  UP: 1,
  RIGHT: 2,
  DOWN: 3,
  KEY_LEFT: 37,
  KEY_UP: 38,
  KEY_RIGHT: 39,
  KEY_DOWN: 40
};

module.exports.Config = Config;

