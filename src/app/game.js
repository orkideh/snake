import {Grid} from './grid';
import {Config} from './config';
import {Logger} from './logger';

class Game {
  constructor() {
    this.grid = new Grid();
    this.ws = window.io('http://localhost:8081/');
    this.logger = new Logger(this.ws, document.getElementById('logs'));
    this.conf = Config;
    this.uiScale = 15;
  }
  run() {
    this.setCanvas();
    document.getElementById('player-name').focus();
    this.setEventListeners();
  }
  setCanvas() {
    this.canvas = document.createElement('canvas');
    this.canvas.width = this.conf.COLS * this.uiScale;
    this.canvas.height = this.conf.ROWS * this.uiScale;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.font = '12px Helvetica';
    this.frames = 0;
  }
  setEventListeners() {
    document.addEventListener('keyup', this.keyupHandler.bind(this));
    document.getElementById('name-submit').addEventListener('click', this.handleSubmitName.bind(this));
    document.getElementById('player-name-form').addEventListener('submit', this.handleSubmitName.bind(this));

    this.ws.on('gameStateUpdate', this.gameStateUpdate.bind(this));
    this.ws.on('loginHandshake', user => this.init(user));
  }
  gameStateUpdate(gameState) {
    this.gameState = gameState;
    this.score = this.gameState.snakes[this.ws.id].score;
  }
  keyupHandler(e) {
    if ([this.conf.KEY_RIGHT, this.conf.KEY_LEFT, this.conf.KEY_DOWN, this.conf.KEY_UP].indexOf(e.keyCode) !== -1) {
      this.ws.emit('updateDirection', e.keyCode);
    }
  }
  handleSubmitName(e) {
    e.preventDefault();
    const nameElem = document.getElementById('player-name');
    if (nameElem.value.trim()) {
      document.getElementById('player-name-container').classList.add('hide');
      this.ws.emit('login', nameElem.value);
    }
  }
  init() {
    this.logger.init();
    document.body.appendChild(this.canvas);
    this.gameOver = false;
    this.grid.init(this.conf.EMPTY, this.conf.COLS, this.conf.ROWS);
    this.loop();
  }
  loop() {
    if (!this.gameOver && this.gameState !== null) {
      this.frames++;
      if (this.frames % 3 === 0) {
        this.draw();
      }
    }
    window.requestAnimationFrame(this.loop.bind(this), this.canvas);
  }
  draw() {
    this.grid.init(this.conf.EMPTY, this.conf.COLS, this.conf.ROWS);
    this.grid.set(this.conf.FRUIT, this.gameState.fruitPos.x, this.gameState.fruitPos.y);

    Object.keys(this.gameState.snakes).forEach(snakeId => {
      this.gameState.snakes[snakeId]._queue.forEach(position => this.setSankeOnGrid(position, snakeId));
    });

    this.grid._grid.forEach((row, rowIndex) => {
      row.forEach((position, colIndex) => this.paintGrid(position, colIndex, rowIndex));
    });
    this.ctx.fillStyle = '#000';
    this.ctx.fillText('SCORE: ' + this.score, 10, this.canvas.height - 10);
  }
  paintGrid(position, colIndex, rowIndex) {
    this.ctx.fillStyle = this.getFillingStyle(position);
    this.ctx.fillRect(rowIndex * this.uiScale, colIndex * this.uiScale, this.uiScale, this.uiScale);
  }
  setSankeOnGrid(position, id) {
    if (this.ws.id === id) {
      this.grid.set(this.conf.SNAKE, position.x, position.y);
    } else {
      this.grid.set(this.conf.GUEST_SNAKE, position.x, position.y);
    }
  }
  getFillingStyle(position) {
    if (position === this.conf.SNAKE) {
      return '#0ff';
    } else if (position === this.conf.GUEST_SNAKE) {
      return '#0f0';
    } else if (position === this.conf.FRUIT) {
      return '#f00';
    }
    return '#fff';
  }
}

Object.assign(Game.prototype, {
  canvas: null,
  ctx: null,
  frames: null,
  fruitPos: null,
  gameOver: null,
  gameState: null,
  uiScale: null,
  score: 0
});

module.exports.Game = Game;

